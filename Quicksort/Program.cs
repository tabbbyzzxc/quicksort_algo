﻿static void Quick_Sort(int[] arr, int left, int right)
{
    if (left < right)
    {
        var pivot = Partition(arr, left, right);

        if (pivot > 1)
        {
            Quick_Sort(arr, left, pivot - 1);
        }
        if (pivot + 1 < right)
        {
            Quick_Sort(arr, pivot + 1, right);
        }
    }
}

static int Partition(int[] arr, int left, int right)
{
    var pivot = arr[left];
    while (true)
    {

        while (arr[left] < pivot)
        {
            left++;
        }

        while (arr[right] > pivot)
        {
            right--;
        }

        if (left < right)
        {
            if (arr[left] == arr[right])
            {
                return right;
            }

            var temp = arr[left];
            arr[left] = arr[right];
            arr[right] = temp;
        }
        else
        {
            return right;
        }
    }
}

static int[] GenerateArray(int length)
{
    var arr = new int[length];
    var random = new Random();
    for (int i = 0; i < length; i++)
    {
        arr[i] = random.Next(0, 10000);
    }

    return arr;
}

Console.Write("Input array length: ");
if(!int.TryParse(Console.ReadLine(), out var length))
{
    Console.WriteLine("Invalid input");
    return;
}

var arr = GenerateArray(length);

Console.WriteLine("Original array : ");
foreach (var item in arr)
{
    Console.Write(" " + item);
}

Console.WriteLine();

Quick_Sort(arr, 0, arr.Length - 1);

Console.WriteLine();
Console.WriteLine("Sorted array : ");

foreach (var item in arr)
{
    Console.Write(" " + item);
}

Console.WriteLine();
                   